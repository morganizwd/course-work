﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace ConsoleApp1
{
    internal class Program
    {
        public static int count = 0;
        static bool IsAdmin;
        static bool IsProgramStopped = false;
        static string adminPasword;
        static string adminPasswordPath = "AdminPassword.txt";
        static string emplyeeBasePath = "EmployeeBase.txt";
        public static Employee[] employeeBase = new Employee[999];

        static void Main(string[] args)
        {
            LoadEmployeeBase();

            while (!IsProgramStopped)
            {
                if (IsAdmin)
                {
                    PrintAdnimMenu();

                    AdminOperations choice = (AdminOperations)int.Parse(Console.ReadLine());

                    switch (choice) 
                    {
                        case AdminOperations.FindEmployee:
                            Console.Clear();
                            FindEmployee();
                            break;

                        case AdminOperations.ShowEmployeeList:
                            Console.Clear();
                            PrintList(employeeBase);
                            break;

                        case AdminOperations.CountMonthPayment:
                            Console.Clear();
                            Console.WriteLine("Enter number of the month");
                            int month;

                            while (!int.TryParse(Console.ReadLine(), out month) || month >= 13 || month <= 0)
                            {
                                Console.WriteLine("Icorrect input... Try again!");
                            }

                            Console.WriteLine($"Month sum is {PaymentCalculator(month)}");
                            break;

                        case AdminOperations.ShowCityList:
                            Console.Clear();
                            Console.WriteLine("Enter month x and y: ");

                            int x;

                            while (!int.TryParse(Console.ReadLine(), out x) || x <= 0 || x >= 13)
                            {
                                Console.WriteLine("Incorrect input... Try again!");
                            }

                            int y;

                            while (!int.TryParse(Console.ReadLine(), out y) || y <= 0 || y >= 13)
                            {
                                Console.WriteLine("Incorrect input... Try again!");
                            }

                            PrintCityList(employeeBase, x, y);
                            break;

                        case AdminOperations.StopPorgram:
                            SaveEmployeeBase();
                            IsProgramStopped = true;
                            break;

                        case AdminOperations.Login:
                            Console.Clear();

                            if (!IsAdmin)
                            {
                                CheckPassword();
                            }
                            else
                            {
                                Console.WriteLine("You're already an admin");
                            }
                            break;

                        case AdminOperations.AddEmployee:
                            Console.Clear();
                            AddEmployee();
                            break;

                        case AdminOperations.RemoveEmplyee:
                            RemoveEmployee();
                            break;

                        case AdminOperations.EditEmployee:
                            Console.Clear();
                            EditEmployee();
                            break;

                        default:
                            Console.Clear();
                            Console.WriteLine("Unknown opeartion... Try again!");
                            break;
                    }
                }
                else
                {
                    PrintMenu();

                    UserOperations choise = (UserOperations)int.Parse(Console.ReadLine());

                    switch (choise)
                    {
                        case UserOperations.FindEmployee:
                            Console.Clear();
                            FindEmployee();
                            break;

                        case UserOperations.ShowEmployeeList:
                            Console.Clear();
                            PrintList(employeeBase);
                            break;

                        case UserOperations.CountMonthPayment:
                            Console.Clear();
                            Console.WriteLine("Enter number of the month");
                            int month;

                            while (!int.TryParse(Console.ReadLine(), out month) || month >= 13 || month <= 0)
                            {
                                Console.WriteLine("Icorrect input... Try again!");
                            }

                            Console.WriteLine($"Month sum is {PaymentCalculator(month)}");
                            break;

                        case UserOperations.ShowCityList:
                            Console.Clear();
                            Console.WriteLine("Enter month x and y: ");

                            int x;

                            while (!int.TryParse(Console.ReadLine(), out x) || x <= 0 || x >= 13)
                            {
                                Console.WriteLine("Incorrect input... Try again!");
                            }

                            int y;

                            while (!int.TryParse(Console.ReadLine(), out y) || y <= 0 || y >= 13)
                            {
                                Console.WriteLine("Incorrect input... Try again!");
                            }

                            PrintCityList(employeeBase, x, y);
                            break;

                        case UserOperations.StopPorgram:
                            SaveEmployeeBase();
                            IsProgramStopped = true;
                            break;

                        case UserOperations.Login:
                            Console.Clear();

                            if (!IsAdmin)
                            {
                                CheckPassword();
                            }
                            else
                            {
                                Console.WriteLine("You're already an admin");
                            }
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine("Unknown opeartion... Try again!");
                            break;
                    }
                }
            } 
        }

        public static void FindEmployee()
        {
            Console.WriteLine("Enter some information about employee you want to find");

            Console.WriteLine("Left line empty if you don't know accurate information");

            Console.Write("Name: ");
            string name = Console.ReadLine();

            Console.Write("Surname: ");
            string surname = Console.ReadLine();

            Console.Write("Last name: ");
            string lastName = Console.ReadLine();

            Console.Write("City: ");
            string city = Console.ReadLine();

            Console.Write("Day: ");
            string day = Console.ReadLine();

            int tempDay = 0;
            int tempMonth = 0;
            int tempTripDuration = 0;
            int tempTripDayPayment = 0;

            if (day != String.Empty)
            {
                if (!int.TryParse(day, out tempDay))
                {
                    tempDay = -1;
                }
            }

            Console.Write("Month: ");
            string month = Console.ReadLine();

            if (month != String.Empty)
            {
                if (!int.TryParse(month, out tempMonth))
                {
                    tempMonth = -1;
                }
            }

            Console.Write("Trip dauration: ");
            string tripDuration = Console.ReadLine();

            if (tripDuration != String.Empty)
            {
                if (!int.TryParse(tripDuration, out tempTripDuration))
                {
                    tempTripDuration = -1;
                }
            }

            Console.Write("Day payment: ");
            string tripDayPayment = Console.ReadLine();

            if (tripDayPayment != String.Empty)
            {
                if (!int.TryParse(tripDayPayment, out tempTripDayPayment))
                {
                    tempTripDayPayment = -1;
                }
            }

            Console.WriteLine();

            for (int i = 0; i < count; i++)
            {
                if (employeeBase[i].name == name || name == String.Empty)
                {
                    if (employeeBase[i].surname == surname || surname == String.Empty)
                    {
                        if (employeeBase[i].lastName == lastName || lastName == String.Empty)
                        {
                            if (employeeBase[i].city == city || city == String.Empty)
                            {
                                if (employeeBase[i].day == tempDay || tempDay == 0)
                                {
                                    if (employeeBase[i].month == tempMonth || tempMonth == 0)
                                    {
                                        if (employeeBase[i].tripDuration == tempTripDuration || tempTripDuration == 0)
                                        {
                                            if (employeeBase[i].tripDayPayment == tempTripDuration || tempTripDuration == 0)
                                            {
                                                Console.WriteLine($"\t{i}\n" +
                                                                  $"Name: {employeeBase[i].name}\n" +
                                                                  $"Surname: {employeeBase[i].surname}\n" +
                                                                  $"Last name: {employeeBase[i].lastName}\n" +
                                                                  $"Start day: {employeeBase[i].day}\n" +
                                                                  $"Month: {employeeBase[i].month}\n" +
                                                                  $"Trip duration: {employeeBase[i].tripDuration}\n" +
                                                                  $"Trip payment: {employeeBase[i].tripDayPayment}\n" +
                                                                  $"City: {employeeBase[i].city}\n");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void SaveEmployeeBase()
        {
            using StreamWriter sw = new StreamWriter(emplyeeBasePath);

            for (int i = 0; i < count; i++)
            {
                sw.WriteLine($"{employeeBase[i].name} {employeeBase[i].surname} {employeeBase[i].lastName} {employeeBase[i].day} {employeeBase[i].month} {employeeBase[i].tripDuration} {employeeBase[i].city} {employeeBase[i].tripDayPayment}");
            }
        }

        public static void LoadEmployeeBase()
        {
            using StreamReader sr = new StreamReader(emplyeeBasePath);

            while (sr.Peek() != -1)
            {
               string[] strings = sr.ReadLine().Split(" ");

                employeeBase[count].name = strings[0];
                employeeBase[count].surname = strings[1];
                employeeBase[count].lastName = strings[2];
                employeeBase[count].day = int.Parse(strings[3]);
                employeeBase[count].month = int.Parse(strings[4]);
                employeeBase[count].city = strings[6];
                employeeBase[count].tripDuration = int.Parse(strings[5]);
                employeeBase[count].tripDayPayment = int.Parse(strings[7]);
                count++;
            }
        }

        public static void PrintCityList(Employee[] employeeBase, int x, int y)
        {
            IDictionary<string, int> cityCounts = new Dictionary<string, int>();

            for (int i = x; i <= y; i++)
            {
                for (int j = 0; j < employeeBase.Length; j++)
                {
                    if (employeeBase[j].month == i)
                    {
                        if (cityCounts.ContainsKey(employeeBase[j].city))
                        {
                            cityCounts[employeeBase[j].city]++;
                        }
                        else
                        {
                            cityCounts.Add(employeeBase[j].city, 1);
                        }
                    }
                }
            }

            foreach (var cityCount in cityCounts)
            {
                Console.WriteLine($"City: {cityCount.Key}\t Visited: {cityCount.Value} time(s)");
            }
        }

        public static double PaymentCalculator(int month)
        {
            double commonPayment = 0;

            for (int i = 0; i < employeeBase.Length; i++)
            {
                if (employeeBase[i].month == month)
                {
                    if (employeeBase[i].tripDuration + employeeBase[i].day >= 30)
                    {
                        commonPayment += employeeBase[i].tripDayPayment * (30 - employeeBase[i].day);
                    }
                    else
                    {
                        commonPayment += employeeBase[i].tripDayPayment * (employeeBase[i].tripDuration - employeeBase[i].day);
                    }
                }
            }

            return commonPayment;
        }

        public static void AddEmployee()
        {
            Console.WriteLine("Enter employee's name:");
            string name = Console.ReadLine();

            employeeBase[count].name = name;

            Console.WriteLine("Enter employee's surname:");
            string surname = Console.ReadLine();

            employeeBase[count].surname = surname;

            Console.WriteLine("Enter employee's last name:");
            string lastName = Console.ReadLine();

            employeeBase[count].lastName = lastName;

            Console.WriteLine("Enter trip city:");
            string city = Console.ReadLine();

            employeeBase[count].city = city;

            Console.WriteLine("Enter day: ");
            int day;

            while(!int.TryParse(Console.ReadLine(), out day) || day <= 0 || day >= 31)
            {
                Console.WriteLine("Incorrect input... try again");
            }

            employeeBase[count].day = day;

            Console.WriteLine("Enter month:");
            int month;

            while (!int.TryParse(Console.ReadLine(), out month) || month <= 0 || month >= 13)
            {
                Console.WriteLine("Incorrect input... try again");
            }

            employeeBase[count].month = month;

            Console.WriteLine("Enter trip duration:");
            int tripDuration;

            while (!int.TryParse(Console.ReadLine(), out tripDuration) || tripDuration <= 0)
            {
                Console.WriteLine("Incorrect input... try again");
            }

            employeeBase[count].tripDuration = tripDuration; 

            Console.WriteLine("Enter day payment:");
            double tripDayPayment;

            while (!double.TryParse(Console.ReadLine(), out tripDayPayment) || tripDayPayment <= 0)
            {
                Console.WriteLine("Incorrect input... try again");
            }

            employeeBase[count].tripDayPayment = tripDayPayment;

            count++;
        }

        public static void RemoveEmployee()
        {
            Console.WriteLine("Enter employee's id:");

            int id;

            while (!int.TryParse(Console.ReadLine(), out id) || id < 0 || id > count - 1)
            {
                Console.WriteLine("Incorrect input... try again");
            }

            for (int i = id; i < count - 1; i++)
            {
                employeeBase[i].name = employeeBase[i + 1].name;
                employeeBase[i].surname = employeeBase[i + 1].surname;
                employeeBase[i].lastName = employeeBase[i + 1].lastName;
                employeeBase[i].day = employeeBase[i + 1].day;
                employeeBase[i].month = employeeBase[i + 1].month;
                employeeBase[i].city = employeeBase[i + 1].city;
                employeeBase[i].tripDuration = employeeBase[i + 1].tripDuration;
                employeeBase[i].tripDayPayment = employeeBase[i + 1].tripDayPayment;
            }

            count--;
        }

        public static void EditEmployee()
        {
            PrintList(employeeBase);

            Console.WriteLine("Enter id of employee you want to edit: ");

            int id = int.Parse(Console.ReadLine());

            Console.Clear();
            Console.WriteLine($"\t{id}\n" +
                                  $"Name: {employeeBase[id].name}\n" +
                                  $"Surname: {employeeBase[id].surname}\n" +
                                  $"Last name: {employeeBase[id].lastName}\n" +
                                  $"Start day: {employeeBase[id].day}\n" +
                                  $"Month: {employeeBase[id].month}\n" +
                                  $"Trip duration: {employeeBase[id].tripDuration}\n" +
                                  $"Trip payment: {employeeBase[id].tripDayPayment}\n" +
                                  $"City: {employeeBase[id].city}\n");

            Console.WriteLine("Left line empty if you dont want to change it");

            Console.Write("Name: ");
            string name = Console.ReadLine();

            if (name != String.Empty)
            {
                employeeBase[id].name = name;
            }

            Console.Write("Surname: ");
            string surname = Console.ReadLine();

            if (surname != String.Empty)
            {
                employeeBase[id].surname = surname;
            }

            Console.Write("Last name: ");
            string lastName = Console.ReadLine();

            if (lastName != String.Empty)
            {
                employeeBase[id].lastName = lastName;
            }

            Console.Write("Day: ");
            string day = Console.ReadLine();

            if (day != String.Empty)
            {
                employeeBase[id].day = int.Parse(day);
            }

            Console.Write("Month: ");
            string month = Console.ReadLine();

            if (month != String.Empty)
            {
                employeeBase[id].month = int.Parse(month);
            }

            Console.Write("Trip dauration: ");
            string tripDuration = Console.ReadLine();

            if (tripDuration != String.Empty)
            {
                employeeBase[id].tripDuration = int.Parse(tripDuration);
            }

            Console.Write("Day payment: ");
            string tripDayPayment = Console.ReadLine();

            if (tripDayPayment != String.Empty)
            {
                employeeBase[id].tripDayPayment = int.Parse(tripDayPayment);
            }

            Console.Write("City: ");
            string city = Console.ReadLine();

            if (city != String.Empty)
            {
                employeeBase[id].city = city;
            }
        }

        static void PrintList(Employee[] employeeBase)
        {
            int counter = 0;

            for (int i = 0; i < count; i++)
            {
                Console.WriteLine($"\t{counter++}\n" +
                                  $"Name: {employeeBase[i].name}\n" +
                                  $"Surname: {employeeBase[i].surname}\n" +
                                  $"Last name: {employeeBase[i].lastName}\n" +
                                  $"Start day: {employeeBase[i].day}\n" +
                                  $"Month: {employeeBase[i].month}\n" +
                                  $"Trip duration: {employeeBase[i].tripDuration}\n" +
                                  $"Trip payment: {employeeBase[i].tripDayPayment}\n" +
                                  $"City: {employeeBase[i].city}\n");
            }
        }

        static void PrintMenu()
        {
            Console.WriteLine("1) Find employee");
            Console.WriteLine("2) Show employee list");
            Console.WriteLine("3) Show month payment");
            Console.WriteLine("4) Show city list");
            Console.WriteLine("5) Login as an admin");
            Console.WriteLine("6) Exit");
        }

        static void PrintAdnimMenu()
        {
            Console.WriteLine("1) Find employee");
            Console.WriteLine("2) Show employee list");
            Console.WriteLine("3) Show month payment");
            Console.WriteLine("4) Show city list");
            Console.WriteLine("5) Login as an admin");
            Console.WriteLine("6) Save changes and exit");
            Console.WriteLine("7) Add employee");
            Console.WriteLine("8) Remove employee");
            Console.WriteLine("9) Edit employee");
        }

        static void CheckPassword()
        {
            using StreamReader sr = new StreamReader(adminPasswordPath);

            adminPasword = sr.ReadLine();

            Console.WriteLine("Enter the pasword");

            if (adminPasword == Console.ReadLine())
            {
                IsAdmin = true;
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Wrong Pasword!");
            }
        }
    }
}