﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public struct Employee
    {
        public string name;

        public string surname;

        public string lastName;

        public int day;

        public int month;

        public int tripDuration;

        public string city;

        public double tripDayPayment;
    }
}
